import torch
from torch.utils.data import Dataset
import os
import json
import numpy as np
from PIL import Image
import imageio

# From dataset directory, read images, poses, and focal lengths (and additional infos, if required)
def read_json(root_dir, split, img_downscale):
    # ------- Load images and poses just as previously
    rgbs = []
    poses = []
    
    # Extract information from .json file
    with open(os.path.join(root_dir, 'transforms_{}.json'.format(split)), 'r') as fp:
        meta = json.load(fp)
    
    # Extract image and corresponding transformation matrix
    imgs = []
    poses = []
    infos = []
    for frame in meta['frames']:
        file_path = frame['file_path']
        if file_path[-4:] != '.png':
            file_path = file_path + '.png'
        fname = os.path.join(root_dir, file_path)
        
        #img = Image.open(fname).convert('RGB')
        
        img = imageio.imread(fname)
        height = img.shape[0] // img_downscale
        width = img.shape[1] // img_downscale
        img = Image.fromarray(img).convert("RGBA")
        
        # add white background
        new_img = Image.new('RGBA', img.size, 'WHITE')
        new_img.paste(img, mask=img)
        img = new_img.convert('RGB')
        
        if img_downscale != 1:
            img = img.resize((height, width), Image.LANCZOS)

        imgs.append(np.array(img))
        poses.append(np.array(frame['transform_matrix']))
        
        # Get additional information per frame, except file path and transformation matrix.
        info = {}
        for key in frame.keys():
            if key != 'file_path' and key != 'transform_matrix':
                info[key] = frame[key]
        infos.append(info)

    rgbs = (np.array(imgs) / 255.).astype(np.float32)
    poses = np.array(poses).astype(np.float32)
    
    h, w = rgbs[0].shape[:2]

    # Load or compute focal lengths.
    focal_x = 0.0
    focal_y = 0.0
    if 'fl_x' in meta:
        focal_x = float(meta['fl_x'])
        focal_y = float(meta['fl_y']) if 'fl_y' in meta else focal_x

        # only rescale focal length, when it was preset and not computed from new w and h values
        focal_x /= img_downscale
        focal_y /= img_downscale
    else:
        camera_angle_x = float(meta['camera_angle_x'])
        camera_angle_y = float(meta['camera_angle_y']) if 'camera_angle_y' in meta else camera_angle_x

        focal_x = .5 * w / np.tan(.5 * camera_angle_x)
        focal_y = .5 * h / np.tan(.5 * camera_angle_y)

    return rgbs, poses, infos, focal_x, focal_y

class NeRFSyntheticDataset(Dataset):
    def __init__(self,
                 root_dir,
                 split='train',
                 additional_info=False,
                 img_downscale=1):
        self.additional_info = additional_info

        self.rgbs, self.poses, self.infos, self.focal_x, self.focal_y = read_json(root_dir, split, img_downscale)
        self.h, self.w = self.rgbs[0].shape[:2]
            
    def __len__(self):
        return self.rgbs.shape[0]
    
    def __getitem__(self, idx):
        if self.additional_info:
            return {
                'rgb': self.rgbs[idx],
                'pose': self.poses[idx],
                'focal_x': self.focal_x,
                'focal_y': self.focal_y,
                'info': self.infos[idx]
            }
        else:
            return {
                'rgb': self.rgbs[idx],
                'pose': self.poses[idx],
                'focal_x': self.focal_x,
                'focal_y': self.focal_y
            }

################# RAYS VERSION OF DATASET ################# 

# Find origin and direction of rays through every pixel, given one camera position.
# Uses the pinhole camera model.
#   c2w:    camera-to-world transformation matrix (= pose)
#   tile_start: [2]-array describing the first pixel position of image tile; if None, uses whole image
def get_rays(c2w, h, w, focal_x, focal_y):
    # --- Shoot parallel rays through every pixel away from camera.
    off = 0.5
    i, j = torch.meshgrid(torch.arange(w, dtype=torch.float32), 
                          torch.arange(h, dtype=torch.float32),
                          indexing='xy')

    # Add half pixel offset for the rays to go through the pixels center, and not its corner
    i, j = i + off, j + off
    dirs = torch.stack([ (i-w*0.5) / focal_x,
                        -(j-h*0.5) / focal_y, 
                        -torch.ones_like(i)], 
                        dim=-1)

    # --- Extract rotation and translation out of camera transformation matrix.
    rot_matrix = torch.Tensor(c2w[:3, :3])
    trans_vec = torch.Tensor(c2w[:3, -1])

    # --- Rotate directions dirs to match the camera rotation.
    # --- Origin of rays is translation from origin to camera center (translation part of matrix).
    rays_d = torch.sum(dirs[..., None, :] * rot_matrix, -1)
    rays_o = trans_vec.expand(rays_d.shape)

    return rays_o, rays_d

class NeRFSyntheticDatasetRays(Dataset):
    def __init__(self,
                 root_dir,
                 split='train',
                 additional_info=False,
                 img_downscale=1):
        self.additional_info = additional_info

        # load rgbs and poses, just as previously
        rgbs, poses, self.infos, self.focal_x, self.focal_y = read_json(root_dir, split, img_downscale)
        self.num_img = rgbs.shape[0]
        self.h, self.w = rgbs[0].shape[:2]

        # precompute rays_o, rays_d
        rays = [get_rays(poses[i], self.h, self.w, self.focal_x, self.focal_y) for i in range(poses.shape[0])]
        self.rays_o = torch.stack([x for (x, _) in rays]).view(-1, 3)
        self.rays_d = torch.stack([y for (_, y) in rays]).view(-1, 3)

        self.pixel_col = torch.Tensor(rgbs).view(-1, 3) # rgbs = [num_images x h x w x 3]
            
    def __len__(self):
        return self.rays_o.shape[0]
    
    def get_h_w(self):
        return self.h, self.w
    
    def get_focal(self):
        return self.focal_x, self.focal_y
    
    def get_num_img(self):
        return self.num_img
    
    def __getitem__(self, idx):
        if self.additional_info:
            return {
                'rays_o': self.rays_o[idx],
                'rays_d': self.rays_d[idx],
                'pixel_cols': self.pixel_col[idx],
                'infos': self.infos[idx // (self.h * self.w)] # infos exists per image
            }
        else:
            return {
                'rays_o': self.rays_o[idx],
                'rays_d': self.rays_d[idx],
                'pixel_cols': self.pixel_col[idx]
            }