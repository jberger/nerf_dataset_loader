## Import Notes
To import .py files, use

`import sys`\
`sys.path.append('path/to/dir')`\
`from nerf_dataset_loader import waymo_dataset as Waymo`\
`from nerf_dataset_loader import nerf_synthetic_dataset as NeRFSynthetic`

## NeRFSynthetic Notes
- the NeRFSyntheticDataset dataset is capable of loading all the synthetic datasets from the original NeRF paper by simply changing the root directory to the desired dataset
- near and far are not part of the dataset and thus need to be set by hand; but, as the scenes are within a [-1, 1] cube, near=2. and far=5. are sufficient
- note, that we can also define the desired split to load, namely train, val, and test

### Data Sample Composition From NeRFSyntheticDataset
'rgb' `[h x w x 3]`: RGB image with white background instead of the transparent one, like in the original image

'pose' `[4 x 4]`: camera to world martrix for the ray computation

'focal_x': focal length per image width, used for ray computation

'focal_y': focal length per image height, used for ray computation

## Waymo Notes
- train dataset is split into blocks of data with ~500-800 images
- each image varies in size
- as there is no "test" split in the original Waymo-dataset, this implementation uses the same images as in train as test images
- blocks correspond to one particular area of the Alamo Square neighborhood, where the data was created
- total of 44 blocks from block_0 to block_43; val has a few images for each block for validation purposes
- in original Block-NeRF, one (compact) NeRF was corresponding to one block
- when using a dataloader on the train data, we get rays through random pixels of a random image

### Data Sample Composition From WaymoDataset
'rays' `[h*w x 10]`: precomputed ray information, concatenated in the last dimension, consisting of
1. rays_o `[h*w x 3]`: ray origins
2. rays_d `[h*w x 3]`: ray directions
3. exposure `[h*w x 1]`: for conditioning Block-NeRF on different exposures
4. radii `[h*w x 1]`: Block-NeRF training radius around Block-NeRF origin
5. near `[h*w x 1]`: nearest distance to camera, where sample points should be computed in
6. far `[h*w x 1]`: furthest distance to camera, where sample points should be computed in

'ts' : `img_idx * torch.ones(len(rays_o), 1)`, image index for each ray

'w_h' `[2]`: [width, height] of image

'rgbs'`[h*w x 3]`: image with the requested index (note: 2d tensor, as height and width were combined)
